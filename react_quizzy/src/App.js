import React, { Component } from "react";
import { Route, Switch } from "react-router";
import { BrowserRouter, NavLink } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { questionslist } from "./questionslist.component";
import { Test } from "./test.component";
import { register } from "./register.component";
import { userprofile } from "./userprofile.component";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <a href="/UserAuthentication" className="navbar-brand">
              Quizzy
            </a>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <NavLink to="/Questions" className="nav-link">
                  All Q&A
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/Test" className="nav-link">
                  Test Yourself
                </NavLink>
              </li>

              <li id="registerheader" className="nav-item">
                <NavLink to="/register" className="nav-link">
                  Register
                </NavLink>
              </li>
            </div>
          </nav>

          <div className="container mt-3">
            <Switch>
              <Route exact path="/UserAuthentication" component={userprofile} />
              <Route exact path="/Questions" component={questionslist} />
              <Route exact path="/Test" component={Test} />
              <Route exact path="/register" component={register} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
