import React, { Component } from "react";
import { variables } from "./Variables";
import { Modal, Button, Form } from "react-bootstrap";

export class questionslist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      modalTitle: "",
      CategoryId_FK: "",
      Question: "",
      QAnswer: "",
      AnswerOption1: "",
      AnswerOption2: "",
      AnswerOption3: "",
      // Question_Id: 0,
    };
  }
  refreshlist() {
    fetch(variables.API_URL + "QuestionAns")
      .then((response) => response.json())
      .then((data) => {
        this.setState({ questions: data });
      });
  }

  componentDidMount() {
    this.refreshlist();
  }

  changeQuestion_ans = (e) => {
    console.log("[LOG] Some changes has been made here");
  };

  closemodal() {
    this.setState({
      isOpen: false,
    });
  }

  addQuestionDBclick() {
    console.log("[LOG] I was triggered during Adding to DB button");

    if (
      this.state.Question === "" ||
      this.state.QAnswer === "" ||
      this.state.AnswerOption1 === "" ||
      this.state.AnswerOption2 === "" ||
      this.state.AnswerOption3 === "" ||
      this.state.CategoryId_FK === ""
    ) {
      alert("Sorry, All fields are mandatory : please add them");
    } else {
      fetch(variables.API_URL + "QuestionAns", {
        method: "POST",
        headers: {
          accept: "application/json",
          "content-type": "application/json",
        },
        body: JSON.stringify({
          CategoryId_FK: this.state.CategoryId_FK,
          Question: this.state.Question,
          QAnswer: this.state.QAnswer,
          AnswerOption1: this.state.AnswerOption1,
          AnswerOption2: this.state.AnswerOption2,
          AnswerOption3: this.state.AnswerOption3,
        }),
      })
        .then((res) => res.json())
        .then((result) => {
          alert(result);
          this.refreshlist();
        });

      this.setState({
        isOpen: false,
      });
    }
  }

  addClick() {
    console.log("I was triggered during add click button");
    this.setState({
      modalTitle: "Add Question",
      Question: "",
      QAnswer: "",
      AnswerOption1: "",
      AnswerOption2: "",
      AnswerOption3: "",
      isOpen: true,
    });
  }

  deleteClick(Id) {
    console.log("Deleting initiated for ID " + Id);

    if (window.confirm("Are you sure?")) {
      fetch(variables.API_URL + "QuestionAns/" + Id, {
        method: "DELETE",
        headers: {
          accept: "application/json",
          "content-type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((result) => {
          alert(result);
          this.refreshlist();
        });
    }
  }

  render() {
    const { questions, modalTitle } = this.state;
    return (
      <div>
        <button
          type="button"
          className="btn btn-primary m-2 float"
          data-bs-toggle="modal"
          data-bs-target="#addModal"
          onClick={() => this.addClick()}
        >
          Add Question
        </button>

        <table className="table table-striped">
          <thead>
            {/* <tr>
              <th>Category</th>

              <th>Question</th>
            </tr> */}

            <tr>
              <th>Category Name</th>
              <th>Question</th>
              <th>Option A</th>
              <th>Option B</th>
              <th>Option C</th>
              <th>Option D</th>
            </tr>
          </thead>
          <tbody>
            {questions.map((quest) => (
              <tr key={quest.Id}>
                <td>{quest.CategoryName}</td>
                <td>{quest.Question}</td>
                <td>{quest.AnswerOption3}</td>
                <td>{quest.AnswerOption1}</td>
                <td>{quest.AnswerOption2}</td>
                <td>{quest.QAnswer}</td>
                <td>
                  <button
                    type="button"
                    className="btn btn-light mr-1"
                    onClick={() => this.deleteClick(quest.Id)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fillrule-rule="currentColor"
                      className="bi bi-trash"
                      viewBox="0 0 16 16"
                    >
                      <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                      <path
                        fillRule-rule="evenodd"
                        d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                      />
                    </svg>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>

        <Modal show={this.state.isOpen}>
          <Modal.Header>
            <Modal.Title>{modalTitle}</Modal.Title>
            <Button variant="secondary" onClick={() => this.closemodal()}>
              Close
            </Button>
          </Modal.Header>
          <Modal.Body>
            <Form onChange={this.changeQuestion_ans}>
              <div
                onChange={(saveData) => {
                  this.setState({ CategoryId_FK: saveData.target.value });
                }}
              >
                <label className="options">
                  Phoenix
                  <input value="1" type="radio" name="Category" required />
                  <span className="checkmark"></span>
                </label>
                <label className="options">
                  {" "}
                  GRD
                  <input value="2" type="radio" name="Category" required />{" "}
                  <span className="checkmark"></span>
                </label>
              </div>
              <br></br>
              <Form.Group className="mb-3" controlId="Form.ControlNew_Question">
                <Form.Label>
                  <strong>Question</strong>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="Question"
                  value={this.state.Question}
                  onChange={(saveData) => {
                    this.setState({ Question: saveData.target.value });
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Form.ControlOption_1">
                <Form.Label>
                  <strong>Answer</strong>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="QAnswer"
                  value={this.state.QAnswer}
                  onChange={(saveData) => {
                    this.setState({ QAnswer: saveData.target.value });
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Form.ControlOption_1">
                <Form.Label>Option 2</Form.Label>
                <Form.Control
                  type="text"
                  name="AnswerOption1"
                  value={this.state.AnswerOption1}
                  onChange={(saveData) => {
                    this.setState({ AnswerOption1: saveData.target.value });
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Form.ControlOption_2">
                <Form.Label>Option 3</Form.Label>
                <Form.Control
                  type="text"
                  name="AnswerOption2"
                  value={this.state.AnswerOption2}
                  onChange={(saveData) => {
                    this.setState({ AnswerOption2: saveData.target.value });
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Form.ControlOption_3">
                <Form.Label>Option 4</Form.Label>
                <Form.Control
                  type="text"
                  name="AnswerOption3"
                  value={this.state.AnswerOption3}
                  onChange={(saveData) => {
                    this.setState({ AnswerOption3: saveData.target.value });
                  }}
                />
              </Form.Group>
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button
              type="submit"
              variant="secondary"
              onClick={() => this.addQuestionDBclick()}
            >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
