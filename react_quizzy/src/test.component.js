import React, { Component } from "react";
import { variables } from "./Variables";

export class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TestQuestions: [],
      id: "",
      QAnswer: "", //Actual  answer for the question
      UAnswer: "", //current users answer
      score: 0, //holds the score
      attempted: 0, // attempted questions
      incorrectAns: 0,
      avgscore: 0,
      retest: false, // determines the status of the buttons
    };
  }
  refreshlist() {
    fetch(variables.API_URL + "TestQuestion")
      .then((response) => response.json())
      .then((data) => {
        this.setState({ TestQuestions: data });
      });
  }

  componentDidMount() {
    this.refreshlist();
  }

  checkAnswer() {
    console.log("I was triggered during Check");
    console.log("Radio button clicked/changed for ID " + this.state.Id);
    console.log("Actual answer :" + this.state.QAnswer);
    console.log("User Answered :" + this.state.UAnswer);
    if (this.state.UAnswer === "") {
      alert("Please click on any of the option and try again");
    } else {
      if (this.state.QAnswer === this.state.UAnswer) {
        this.setState({
          score: this.state.score + 1,
        });
      } else {
        this.setState({
          incorrectAns: this.state.incorrectAns + 1,
        });
      }
      this.setState(
        {
          attempted: this.state.attempted + 1,
        },
        this.checkfinish
      );

      document.getElementById(this.state.Id).style.display = "none";
      document.getElementById("scoreboard").style.display = "block";
      // this.checkfinish();
    }
  }

  checkfinish() {
    if (
      this.state.attempted === 5 ||
      this.state.score + this.state.incorrectAns === 5
    ) {
      console.log("Quiz finshed");
      console.log("User Answered :" + this.state.score);

      this.setState({
        avgscore: (this.state.score / 5) * 100,
      });

      document.getElementById("finish").style.display = "block";

      // fetch(variables.API_URL + "QuestionAns", {
      //   method: "POST",
      //   headers: {
      //     accept: "application/json",
      //     "content-type": "application/json",
      //   },
      //   body: JSON.stringify({
      //     CategoryId_FK: this.state.CategoryId_FK,
      //     Question: this.state.Question,
      //     QAnswer: this.state.QAnswer,
      //     AnswerOption1: this.state.AnswerOption1,
      //     AnswerOption2: this.state.AnswerOption2,
      //     AnswerOption3: this.state.AnswerOption3,
      //   }),
      // })
      //   .then((res) => res.json())
      //   .then((result) => {
      //     alert(result);
      //     this.refreshlist();
      //   });
    }
  }

  render() {
    const { TestQuestions } = this.state;

    return (
      <div>
        <div id="scoreboard">
          <div>
            <span id="">
              {this.state.score}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fillrule-rule="currentColor"
                className="bi bi-check"
                viewBox="0 0 16 16"
                id="check"
              >
                <text
                  x="0"
                  y="50"
                  fontFamily="Verdana"
                  fontSize="35"
                  fillrule-rule="blue"
                ></text>
                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                {this.state.score}
              </svg>
            </span>
            <span id="">
              {this.state.incorrectAns}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fillrule-rule="currentColor"
                className="bi bi-x-lg"
                viewBox="0 0 16 16"
                id="incorrect"
              >
                <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z" />
              </svg>
            </span>
          </div>
        </div>

        <div id="finish">
          <span>
            <p>The Test is finished your score is {this.state.avgscore}%.</p>
            <div className="d-flex align-items-center pt-3">
              <div className="ml-auto mr-sm-5"> </div>
            </div>
          </span>
        </div>

        {TestQuestions.map((Test, index) => (
          <div id={Test.Id} className="testcontainer mt-sm-5 my-1">
            <div className="question ml-sm-5 pl-sm-5 pt-2">
              <div className="py-2 h5">
                <b>{Test.Question}</b>
              </div>

              <div
                className="ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3"
                id="options"
                //  onChange={() => this.ansRadio(Test.Id, Test.QAnswer)}
                onChange={(checkAnswer) => {
                  this.setState({
                    Id: Test.Id,
                    QAnswer: Test.QAnswer,
                    UAnswer: checkAnswer.target.value,
                  });
                }}
              >
                <label className="options">
                  {Test.AnswerOption1}
                  <input
                    type="radio"
                    value={Test.AnswerOption1}
                    name={Test.Id}
                  />
                  <span className="checkmark"></span>
                </label>
                <label className="options">
                  {" "}
                  {Test.AnswerOption2}
                  <input
                    type="radio"
                    value={Test.AnswerOption2}
                    name={Test.Id}
                  />{" "}
                  <span className="checkmark"></span>
                </label>
                <label className="options">
                  {Test.AnswerOption3}{" "}
                  <input
                    value={Test.AnswerOption3}
                    type="radio"
                    name={Test.Id}
                  />{" "}
                  <span className="checkmark"></span>{" "}
                </label>
                <label className="options">
                  {Test.QAnswer}
                  <input
                    type="radio"
                    value={Test.QAnswer}
                    name={Test.Id}
                  />{" "}
                  <span className="checkmark"></span>{" "}
                </label>
              </div>
            </div>

            <div className="d-flex align-items-center pt-3">
              <div className="ml-auto mr-sm-5">
                {" "}
                <button
                  id={Test.Id}
                  disabled={this.state.disabled}
                  onClick={() => this.checkAnswer(Test.Id)}
                  className="btn btn-success"
                >
                  Check
                </button>{" "}
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
