import React, { Component } from "react";
import { variables } from "./Variables";
import { Modal, Button, Form } from "react-bootstrap";
import { Route, Switch } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { userprofile } from "./userprofile.component";

export class register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalTitle: "Register",
      name: "",
      email: "",
      role: "employee",
      hashedpass: "",
      password: "",
      Errors: "",
      isOpen: true,
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  closemodal() {
    this.setState({
      isOpen: false,
    });
  }
  addtoDB() {
    console.log("[LOG] I was triggered during Adding to DB button");
    var bcrypt = require("bcryptjs");

    if (
      this.state.name === "" ||
      this.state.email === "" ||
      this.state.role === "" ||
      this.state.password === ""
    ) {
      alert("Sorry, All fields are mandatory : please add them");
    } else {
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(this.state.password, salt);
      console.log("hashed password is " + hash);
      if (hash) {
        fetch(variables.API_URL + "RegisterQuizzy", {
          method: "POST",
          headers: {
            accept: "application/json",
            "content-type": "application/json",
          },
          body: JSON.stringify({
            name: this.state.name,
            role: this.state.role,
            email: this.state.email,
            password: hash,
          }),
        }).then((res) => res.json());
        // .then((result) => {
        //   alert(result);
        // });

        this.setState({
          isOpen: false,
        });
        document.getElementById("finish").style.display = "block";
      } else {
        alert("please try again, an error occured during encryption");
      }
    }
  }

  render() {
    const { modalTitle } = this.state;
    return (
      <div className="Login">
        <div id="finish">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fillrule-rule="currentColor"
            className="bi bi-check"
            viewBox="0 0 16 16"
            id="check_register"
          >
            <text
              x="0"
              y="50"
              fontFamily="Verdana"
              fontSize="35"
              fillrule-rule="blue"
            ></text>
            <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
            {this.state.score}
          </svg>
          <span>
            <BrowserRouter>
              <div class="alert alert-success" role="alert">
                Welcome {this.state.name}. Registration completed successfully.
              </div>
              <div>
                Click here to{" "}
                <a
                  id="login_link"
                  href="/UserAuthentication"
                  className="navbar-brand"
                >
                  Log In
                </a>
                <div className="container mt-3">
                  <Switch>
                    <Route
                      exact
                      path="/UserAuthentication"
                      component={userprofile}
                    />
                  </Switch>
                </div>
              </div>
            </BrowserRouter>
          </span>
        </div>

        <Modal show={this.state.isOpen}>
          <Modal.Header>
            <Modal.Title>{modalTitle}</Modal.Title>
            <Button variant="secondary" onClick={() => this.closemodal()}>
              Close
            </Button>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3" controlId="Form.register">
                <Form.Label>
                  <strong>Name</strong>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={this.state.name}
                  onChange={(saveData) => {
                    this.setState({ name: saveData.target.value });
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Form.register">
                <Form.Label>
                  <strong>Email</strong>
                </Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  value={this.state.email}
                  onChange={(saveData) => {
                    this.setState({ email: saveData.target.value });
                  }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="Form.register">
                <Form.Label>
                  <strong>Password</strong>
                </Form.Label>
                <Form.Control
                  type="text"
                  name="password"
                  value={this.state.password}
                  onChange={(saveData) => {
                    this.setState({ password: saveData.target.value });
                  }}
                />
              </Form.Group>
            </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button
              type="submit"
              variant="secondary"
              onClick={() => this.addtoDB()}
            >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
