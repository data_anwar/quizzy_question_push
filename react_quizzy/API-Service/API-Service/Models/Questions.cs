﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Service.Models
{
    public class Questions
    {
        public string QuestionDescription { get; set; }
        public int CategoryId_FK { get; set; }
    }
}
