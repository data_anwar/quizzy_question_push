﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using API_Service.Models;


namespace API_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterQuizzyController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public RegisterQuizzyController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]

        public JsonResult Get()
        {

            string Query = @"select *
                             from [Quizzy].[dbo].[UserLogin]
                             ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Answer QuestAns)
        {

            string Query = @"INSERT INTO [Quizzy].[dbo].[UserLogin]
                             Values (@Name,@Role,@Email,@Password)";
           

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@Name", QuestAns.Name);
                    myCommand.Parameters.AddWithValue("@Role", QuestAns.Role);
                    myCommand.Parameters.AddWithValue("@Email", QuestAns.Email);
                    myCommand.Parameters.AddWithValue("@Password", QuestAns.Password);
                    
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("The User has been succefully added");
        }

       


    }
}
