﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace API_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestQuestionController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TestQuestionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]

        public JsonResult Get()
        {

            string Query = @"select top 5 a.Id,c.CategoryName,a.Question,a.QAnswer,a.AnswerOption1,a.AnswerOption2,a.AnswerOption3
                             from Answer a
                             Join Category c ON c.Id=a.CategoryId
							 ORDER by NEWID() ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }






    }


}
