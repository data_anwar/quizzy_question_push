﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using API_Service.Models;


namespace API_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        public RegisterController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        [HttpGet]
        public JsonResult Get()
        {

            string Query = @"select top 5 a.Id,c.CategoryName,a.Question,a.QAnswer,a.AnswerOption1,a.AnswerOption2,a.AnswerOption3
                             from Answer a
                             Join Category c ON c.Id=a.CategoryId
							 ORDER by NEWID() ";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }



        [HttpPost]
        public JsonResult Post(Answer Test)
        {

            string Query = @"INSERT INTO [Quizzy].[dbo].[UserLogin]
                             Values (@Name,@Role,@Email,@Password)";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@Name", Test.Name);
                    myCommand.Parameters.AddWithValue("@Role", Test.Role);
                    myCommand.Parameters.AddWithValue("@Email", Test.Email);
                    myCommand.Parameters.AddWithValue("@Password", Test.Password);

                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("The User has been succefully added");
        }
    }



}
