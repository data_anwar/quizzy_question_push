﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using API_Service.Models;


namespace API_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionAnsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public QuestionAnsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]

        public JsonResult Get()
        {

            string Query = @"select a.Id,c.CategoryName,a.Question,a.QAnswer,a.AnswerOption1,a.AnswerOption2,a.AnswerOption3
                             from Answer a
                             Join Category c ON c.Id=a.CategoryId";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Answer QuestAns)
        {

            string Query = @"INSERT INTO [Quizzy].[dbo].[Answer]
                             Values (@CategoryId_FK,@Question,@QAnswer,@AnswerOption1,@AnswerOption2,@AnswerOption3)";
           

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@CategoryId_FK", QuestAns.CategoryId_FK);
                    myCommand.Parameters.AddWithValue("@Question", QuestAns.Question);
                    myCommand.Parameters.AddWithValue("@QAnswer", QuestAns.QAnswer);
                    myCommand.Parameters.AddWithValue("@AnswerOption1", QuestAns.AnswerOption1);
                    myCommand.Parameters.AddWithValue("@AnswerOption2", QuestAns.AnswerOption2);
                    myCommand.Parameters.AddWithValue("@AnswerOption3", QuestAns.AnswerOption3);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("The question has been succefully added");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {

            string Query = @"Delete from [Quizzy].[dbo].[Answer]
                             where Id=@id";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("QuizzyAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(Query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("The question has been removed successfully ");
        }


    }
}
