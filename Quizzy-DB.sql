USE [master]
GO

/****** Object:  Database [Quizzy]    Script Date: 13/08/2021 6:11:16 pm ******/
CREATE DATABASE [Quizzy]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DanTest', FILENAME = N'C:\Users\AnwarAbdulla\DanTest.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DanTest_log', FILENAME = N'C:\Users\AnwarAbdulla\DanTest_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Quizzy].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Quizzy] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [Quizzy] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [Quizzy] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [Quizzy] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [Quizzy] SET ARITHABORT OFF 
GO

ALTER DATABASE [Quizzy] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [Quizzy] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [Quizzy] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [Quizzy] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [Quizzy] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [Quizzy] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [Quizzy] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [Quizzy] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [Quizzy] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [Quizzy] SET  DISABLE_BROKER 
GO

ALTER DATABASE [Quizzy] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [Quizzy] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [Quizzy] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [Quizzy] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [Quizzy] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [Quizzy] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [Quizzy] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [Quizzy] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [Quizzy] SET  MULTI_USER 
GO

ALTER DATABASE [Quizzy] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [Quizzy] SET DB_CHAINING OFF 
GO

ALTER DATABASE [Quizzy] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [Quizzy] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [Quizzy] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [Quizzy] SET QUERY_STORE = OFF
GO

USE [Quizzy]
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE [Quizzy] SET  READ_WRITE 
GO

